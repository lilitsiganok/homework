
function createNewUser() {
    let firstName = prompt("Enter your First name!");
    let lastName = prompt("Enter your Last name!");
    let birthDstring = prompt("Enter your birthday!", "DD.MM.YYYY");
    let birthD = new Date(birthDstring.slice(6), birthDstring.slice(3, 5) - 1, birthDstring.slice(0, 2));
    return {
        firstName : firstName,
        lastName : lastName,
        birthday : birthD,
        getLogin : function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getPassword : function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        },
        getAge : function () {
            let now = new Date(); //current date
            let bdThisYear = new Date(now.getFullYear(), this.birthday.getMonth(), this.birthday.getDate()); //Birthday this year
            //Age = current year - birth year
            let age = now.getFullYear() - this.birthday.getFullYear();
            if (now < bdThisYear) {
                    age = age-1;
            }
            return age;
        }
    }
}
let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());
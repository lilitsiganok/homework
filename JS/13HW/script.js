const btn = document.querySelector ('.button');
const body = document.getElementsByTagName ('body');
let color = localStorage.getItem ('color');

window.onload = function () {
    if (localStorage.getItem('color') !== null) {
        body[0].classList = color;
}}
btn.addEventListener('click', function(event) {
    if (localStorage.getItem('color')){
        body[0].classList.remove('color-theme');
        localStorage.removeItem('color');
    } else {
        body[0].classList.add('color-theme');
        localStorage.setItem('color', 'purple');
    };
})
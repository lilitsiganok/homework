import React, { Component } from 'react';
import './App.css';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';

class App extends Component{
  constructor(props) {
    super(props)
      this.state = {
        isFirstModal: false,
        isSecondModal: false,
      }
    
  }

  // state = {
  //   isFirstModal: false,
  //   isSecondModal: false,
  // }
 
  handleFirstModal(){this.setState((state)=>({...state, isFirstModal: !state.isFirstModal,}))
  }

handleSecondModal() {
  this.setState((state)=>({
    ...state,
    isSecondModal: !state.isSecondModal,
  }))
}



render() {
  // const modalActions = <div>Button</div>
  return (
    <div>
      <Modal isOpen={this.state.isFirstModal} onCancel={()=>this.handleFirstModal()}
      header="Second modal"
      text="Text"/>
      <Modal
      isOpen={this.state.isSecondModal}
      onCancel={
        ()=>
          this.handleSecondModal()

      }
      header="First modal"
      text="Text"
      // actions={modalActions}
      />

      <Button name="Open first modal" onClick={this.handleFirstModal.bind(this)}/>
      <Button name="Open second modal" onClick={this.handleSecondModal.bind(this)}/>
      {/* <Button name="Open  modal"/> */}

    </div>
  )
}

}

export default App;

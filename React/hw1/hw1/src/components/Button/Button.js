import React, { Component } from 'react';

class Button extends Component {
    constructor(props) {
        super(props)
        this.name = props.name
    }
 render() {
  return(
      <button onClick={this.props.onClick}>{this.name}</button>
    )
   }
 }

export default Button;
import React from 'react'

class Modal extends React.Component {
  

render() {
    
    const {isOpened, style, onModalClose, title, text, children, classNameBack, closeModal} = this.props;
    return (
        <div 
        className={`modal__wrapper  modal-first modal-second ${isOpened ? 'open' : 'close'}`} 
        onClick={closeModal}
        style = {{...style}}
        // className = {classNameBack}
        >

            <div className={classNameBack} onClick={e=>e.stopPropagation()}>
            <div className="modal__close" onClick={onModalClose}>x</div>
           
            <h2>{title}</h2>
            <hr /> 
            <p>{text}</p>
            {children}
           
            
        </div>
       
    </div>
    
    )
}

}
export default Modal;

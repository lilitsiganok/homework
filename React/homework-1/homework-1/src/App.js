import logo from './logo.svg';
import './App.css';
import './Modal.css';
import React from 'react';
import Modal from './Modal';
import Button from './Button';

  class App extends React.Component {
    state ={
       modal1: false,
       modal2: false,
       closeButton: false
    }
  openFirstModal = () => {
    this.setState({modal1: true, modal2: false, closeButton: true});
  }

  openSecondModal = () => {
    this.setState({modal2: true, modal1: false, closeButton: true});
  }
  closeFirstModal = () => {
    this.setState ({modal1: false});
  }
  closeSecondModal = () => {
    this.setState ({modal2: false});
  }
  closePopup = (e) => {
    if (e.target.closest('.modal__wrapper.open') &&  !e.target.closest('.modal__body')) {
        this.closeFirstModal();
        this.closeSecondModal();
    }
};
  
  render ()  {
      return (
        <div className="App">
         
          <header className="App-header">
            <div className="button-container">

            <Button className="btn btn-danger" text='Open first modal' onClick={this.openFirstModal}/>
            <Button className="btn btn-danger" text='Open second modal'  onClick={this.openSecondModal}/>
            </div>
          </header>


          <Modal 
          title = {"If you want to cancel - press 'Cancel"}
          isOpened = {this.state.modal1}
          closeButton={true} 
          onModalClose ={() => this.setState({modal1: false})}
          onClick={e=>e.stopPropagation()}
          text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" 
          className='modal-first modal-second'
          closeModal={this.closePopup}
          classNameBack = {'style1 modal__body'}
          >
          <img src={logo} className="App-logo" alt="logo" />

          <p>{this.text}</p>
            <div className='button-container'>
            <Button className="btn btn-danger" text='Submit' backgroundColor='aquamarine'/>
            <Button className="btn btn-danger" text='Cancel' onClick={this.closeFirstModal} backgroundColor='plum'/> 
            </div>
           
          </Modal>



            <Modal
            title = {"If you press No, all your files will be deleted"}
            isOpened = {this.state.modal2}
            closeButton={true}
            onModalClose ={() => this.setState({modal2: false})}
            onClick={e=>e.stopPropagation()}
            text="Are you sure you want to delete this file?" 
            closeModal={this.closePopup}
            classNameBack={'style2 modal__body'}
            >
            <img src={logo} className="App-logo" alt="logo" />
      
            <p>{this.text}</p>
            <div className='button-container'>
              <Button className="btn btn-danger" text='Ok' backgroundColor=' rgb(27, 233, 164)'/>
              <Button className="btn btn-danger" text='No' onClick={this.closeSecondModal} backgroundColor=' rgb(232, 102, 232)'/>
              
              </div>
            </Modal>

  
    
        </div>
      );
    }

  
}


export default App;

import { ADD_PRODUCTS } from "../constants/addProducts";
import axios from 'axios';

export function getProducts(){
return function(dispatch){
    axios('/database.json')
    .then(result=>{
        if(!result?.data.length)return
        return dispatch({
            type:ADD_PRODUCTS,
            data:result.data,
        })
    })

}
}

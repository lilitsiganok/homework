import { combineReducers} from 'redux';
import { addToCardReducer } from './addToCardReducer';

import allProducts from './addProducts'

export default combineReducers({
  addToCard: addToCardReducer,
  goods: allProducts,
});


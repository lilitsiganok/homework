import { ADD_PRODUCTS } from "../constants/addProducts";

const initialState = {
data: [],
isLoading: true,
error: null,

}
export default function allProducts(state=initialState, action) {
switch (action.type) {
    case ADD_PRODUCTS:{
        return{
            ...state,
            data: action.data,
            isLoading: false

        }

    }
    default: 
    return state;

}
}
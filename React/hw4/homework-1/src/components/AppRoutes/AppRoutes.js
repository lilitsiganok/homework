import React from 'react'
import {Route, Switch} from 'react-router-dom'
import App from '../../App'
import Cart from '../Cart/Cart'
import Error from '../../components/Error/Error'
import Favorites from '../Favorites/Favorites'
import Header from '../Header/Header'

 const AppRoutes = () => {
  return(
    <div>
      <Header className='header'/>
        <Switch>
    <Route exact path="/"><App/></Route>
    <Route exact path="/favorites"> <Favorites/> </Route>
    <Route exact path="/cart"> <Cart/> </Route>
    <Route exact path="*"> <Error/> </Route>
        </Switch>

    </div>
   )

 }
 export default AppRoutes
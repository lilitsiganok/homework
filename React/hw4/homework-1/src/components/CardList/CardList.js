import React, { useEffect } from 'react'
import {useSelector, useDispatch} from 'react-redux'
import Card from '../Card/Card'
import * as goodsActions from '../../store/actions/getProducts'


const CardList =()=> {

const goods = useSelector(state=> state.goods.data)
const dispatch=useDispatch();

useEffect(() => {
    dispatch(goodsActions.getProducts())
}, [])


 
  return(
   <div className="card">
       {goods.map((item) => (
           <Card 
           img={item.image}
           name={item.name}
           color={item.color}
           price={item.price}
           id={item.id}
           saveToCart={true}
           removeFromCart={false}


           />
       ))}

   </div>
    )
   
 }



export default CardList
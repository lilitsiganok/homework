import React, { useEffect , useState} from 'react'
import './Card.scss'
import Button from '../Button/Button'
import Modal from '../Modal/Modal'

const  Card = (props) => {
    const[openModal1, setOpenModal1] = useState(false)
    const[openModal2, setOpenModal2] = useState(false)
    const [starColor, setStarColor] = useState('grey')
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])


useEffect( () => {
    const {id} = props;
    if(favorites?.includes(id)){
        setStarColor('red')
    } else{
        setStarColor('grey')
    }
}, [])

    const clickStar = (id) => {
    const idArr = JSON.parse(localStorage.getItem('favorites')) || []

    if(idArr.includes(id)){
        const index = idArr.indexOf(id) 
        idArr.splice(index, 1)
        localStorage.setItem('favorites', JSON.stringify(idArr))
        setStarColor('grey')
    }else{
        idArr.push(id)
        localStorage.setItem('favorites', JSON.stringify(idArr))
        setStarColor('red')
    }
    
    }

    const addToCart = (id) =>{
        const newArr = JSON.parse(localStorage.getItem('cart')) || []
        newArr.push(id)
        localStorage.setItem('cart', JSON.stringify(newArr))
    }

    const removeItem = (id) => {
        const index = cart.indexOf(id)
        cart.splice(index, 1)
        setCart(cart)
        localStorage.setItem('cart', JSON.stringify(cart))
    }


    const { id, img, name, color, article, price, saveToCart, removeFromCart} = props
  return(
      <>
   <div>
       <img src={img} alt={name} style={{width:'100%', height:'385px'}}/>

       <div className="card-body">
       <div>
       <p className="text-secondary">Name: {name}</p>
       <p className="text-secondary">Color: {color}</p>
       <p className="text-secondary">Price: {price} uan</p>
       <p className="text-secondary">{article}</p>
       <div className ='rounded text-center'>
       <svg onClick={() => clickStar(id)} xmlns="http://www.w3.org/2000/svg" style={{width:'20px', height:'20px'}} className="bi bi-star" viewBox="0 0 16 16">
        <path style={{fill: starColor}} d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288L8 2.223l1.847 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.565.565 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z"/>
                            </svg> 
       </div>
       {/* <Button className='btn btn-light' text={!props.isCart ? "Add to cart" : "Remove from the cart"} onClick={onClickAddToCart}/>
        */}

       {saveToCart && <Button className='btn btn-info' text="Add to cart" onClick={() => (setOpenModal1(true))}/>}
       {removeFromCart && <Button className='btn btn-info' text='Remove' onClick={() => (setOpenModal2(true))}/>}
       {openModal1 && <Modal 
       onClick={() => (setOpenModal1(false))} 
       header='Do you want to save this item?' 
       text="Are you sure you want to save this item?" 
       closeButton={true}
       action={<div className='modal-buttons'>
           <Button className='btn btn-info' text='add' onClick={() => (addToCart(id))}/>
           <Button className='btn btn-info' text='cancel'/>
           </div>} 
       />}
       {openModal2 && <Modal 
       onClick={() => (setOpenModal2(false))} 
       header='Do you want to remove this item?' 
       text="Are you sure you want to remove this item?" 
       closeButton={true}
       action={<div className='modal-buttons'>
           <Button className='btn btn-info' text='remove' onClick={() => (removeItem(id))}/>
           <Button className='btn btn-info' text='cancel'/>
           </div>} 
       />}

       </div>
       </div>

   </div>
   </>
    )
   }




export default Card
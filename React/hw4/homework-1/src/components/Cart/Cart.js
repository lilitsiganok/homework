import React from 'react'
import {useEffect, useState} from 'react'
import axios from 'axios'
import Card from '../Card/Card'
import Button from '../Button/Button'
import {useSelector, useDispatch} from 'react-redux'
import * as goodsActions from '../../store/actions/getProducts'

 const Cart = () => {
  const goods = useSelector(state => state.goods)
  const dispatch=useDispatch();

    // const [goodsInCart, setGoodsInCart] = useState([])
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])

    useEffect(() => {
      dispatch(goodsActions.getProducts())
  }, [])

  const result = goods?.data.filter(item => cart?.includes(item.id.toString()))

    // useEffect (() => {
    //     axios('/database.json').then((response)=>{
    //     const result = response?.data.filter(item => cart?.includes(item.id.toString()))

    //     setGoodsInCart(result)
        
    //     })
    // }, [])

   

    
  return(
    <div className='card'>
    {result.map((item) => (
        <Card 
        img={item.image}
           name={item.name}
           color={item.color}
           price={item.price}
           id={item.id}
           saveToCart={false}
           removeFromCart={true}
           />
    ))}
     

</div>
   )

 }
 export default Cart


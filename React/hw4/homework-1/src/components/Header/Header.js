import React from 'react'
import {NavLink} from 'react-router-dom'

 const Header = () => {
  return(
    <div className="menu">
        <NavLink exact to="/" className="menu-item" activeClassName='active-menu-item'>Нome</NavLink>
        <NavLink exact to="/favorites" className="menu-item" activeClassName='active-menu-item'>Favorites</NavLink>
        <NavLink exact to="/cart"  className="menu-item" activeClassName='active-menu-item'>Cart</NavLink>
    </div>
   )

 }
 export default Header
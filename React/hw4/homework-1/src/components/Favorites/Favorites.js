
import React, { useEffect } from 'react'
import {useState} from 'react'
import Card from '../Card/Card'
import {useSelector, useDispatch} from 'react-redux'
import * as goodsActions from '../../store/actions/getProducts'


const Favorites = () => {

  const goods = useSelector(state => state.goods)
  const dispatch=useDispatch();
  const [idArray, setIdArray] = useState( JSON.parse(localStorage.getItem('favorites') || []))

  
  useEffect(() => {
      dispatch(goodsActions.getProducts())
  }, [])




const result = goods?.data?.filter(item=>idArray?.includes(item.id))
console.log(result)


  return(
    <div className="card">
       {result.map((item) => (
           <Card 
           img={item.image}
           name={item.name}
           color={item.color}
           price={item.price}
           id={item.id}
           saveToCart={true}
           removeFromCart={false}

           />
           
       ))}

   </div>
   )

 }

 export default Favorites
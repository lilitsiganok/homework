import axios from 'axios';
import React, { useEffect } from 'react'
import {useState} from 'react'
import Card from '../Card/Card'


const Favorites = () => {

const [idArray, setIdArray] = useState( JSON.parse(localStorage.getItem('favorites') || []))
const [favoritesArray, setFavoritesArray] = useState([])

useEffect(() => {
axios('/database.json').then(async (response)=>{
    const result = await response?.data?.filter(item =>{
     return idArray?.includes(item.id)
    })
    setFavoritesArray(result)
})
}, [idArray])


  return(
    <div className="card">
       {favoritesArray.map((item) => (
           <Card 
           img={item.image}
           name={item.name}
           color={item.color}
           price={item.price}
           id={item.id}
           saveToCart={true}
           removeFromCart={false}

           />
           
       ))}

   </div>
   )

 }

 export default Favorites
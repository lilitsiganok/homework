import React from 'react'
// import './Button.scss' 

class Button extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      title: '',
    }
  }
  render () { 
    const { className, backgroundColor, text, onClick} = this.props
    return (
      <div>
        <button className={className} style= {{backgroundColor: backgroundColor}} onClick={onClick}>{text}</button>
      </div>
    )
  }
}

export default Button;
import React from 'react'
import {useEffect, useState} from 'react'
import axios from 'axios'
import Card from '../Card/Card'
import Button from '../Button/Button'

 const Cart = () => {

    const [goodsInCart, setGoodsInCart] = useState([])
    const [cart, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || [])


    useEffect (() => {
        axios('/database.json').then((response)=>{
        const result = response?.data.filter(item => cart?.includes(item.id.toString()))

        setGoodsInCart(result)
        
        })
    }, [])

   

    
  return(
    <div className='card'>
    {goodsInCart.map((item) => (
        <Card 
        img={item.image}
           name={item.name}
           color={item.color}
           price={item.price}
           id={item.id}
           saveToCart={false}
           removeFromCart={true}
           />
    ))}
     

</div>
   )

 }
 export default Cart


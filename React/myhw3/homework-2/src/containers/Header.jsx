import React from 'react'
import { NavLink } from 'react-router-dom'

const Header=()=> {
    return (
        <div className="menu">
            <NavLink exact to="/" activeClassName="menu-item  selected">Home</NavLink>
            <NavLink exact to="/cart" activeClassName="menu-item  selected">Cart</NavLink>
            <NavLink exact to="/favorites" activeClassName="menu-item  selected">Favorites</NavLink>
        </div>
    )
}
export default Header;
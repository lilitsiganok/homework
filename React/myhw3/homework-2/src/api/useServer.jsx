import { useEffect, useState } from 'react'

const useServer = () => {
    const [clothes, setClothes] = useState([]);
    useEffect(() => {
        fetch('database.json')
        .then (response => response.json())
        .then (data => setClothes(data))
        ;
      }, [])
      return clothes
}

export default useServer
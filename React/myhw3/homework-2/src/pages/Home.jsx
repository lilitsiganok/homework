import { useEffect, useState } from 'react'
import ProductCard from '../containers/ProductCard'

const Home = () => {
    const [clothes, setClothes] = useState([]);
    useEffect(() => {
        fetch('database.json')
        .then (response => response.json())
        .then (data => setClothes(data))
        ;
      }, [])

      return (

        <ProductCard clothesList={clothes}/>
      
      )
     
}

export default Home
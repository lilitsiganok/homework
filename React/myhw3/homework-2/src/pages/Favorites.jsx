import React, {useState, useEffect} from 'react'
import server from '../api/useServer'
import ProductCard from '../containers/ProductCard'
const Favorites = () => {
    
    const [favsClothes, setFavsClothes] = useState([])
    const articleArr = localStorage.getItem('favs');
    const parsedArr = JSON.parse(articleArr)
    const clothes =  server ()
    
        useEffect(() => {
            if (parsedArr) {
                setFavsClothes(
                    clothes.filter(obj => parsedArr.find(id => id === obj.article))
                )
                
            } else {
                setFavsClothes(false)
            }
        },[clothes, parsedArr])

    return (
        <div>
          {favsClothes && <ProductCard isFav clothesList={favsClothes}/>}
        </div>
    )
}

export default Favorites
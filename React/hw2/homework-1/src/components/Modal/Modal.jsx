import React, { Component } from 'react'
import Button from '../Button/Button'
import PropTypes from 'prop-types'

export class Modal extends Component {

    render() {
        const {header, closeButton, text, onClick, action} = this.props
        return (
        <div className="modal-container"  onClick={onClick}>
            <div className="modal-dialog modal-dialog-centered">
              <div className='modal-content'>
                <div className='modal-header'>
                  <h5 className="modal-title">{header}</h5>
                  {closeButton && <Button className="close" text='X' onClick={onClick} />}
                </div>
                <div className="modal-body">
                  <p>{text}</p>
                </div>
                <div className="modal-footer">
                  {action}
                </div>
              </div>
            </div>
        </div> 
        )
    }
}

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  stopPropagation: PropTypes.func,
  onClick: PropTypes.func,
  action: PropTypes.element,
};

export default Modal

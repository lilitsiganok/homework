import React, { Component } from 'react'
import axios from 'axios'
import Card from '../Card/Card'


class CardList extends Component {

 state = {
     goods: [],
 }
 componentDidMount(){
     axios('/database.json').then((response)=>{
         this.setState({goods: response.data})
     })
    
 }

 render() {
     const {goods} = this.state
  return(
   <div className="card">
       {goods.map((item) => (
           <Card 
           img={item.image}
           name={item.name}
           color={item.color}
           price={item.price}
           id={item.id}


           />
       ))}

   </div>
    )
   }
 }



export default CardList
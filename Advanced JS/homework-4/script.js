const urlApi = 'https://ajax.test-danit.com/api/swapi/films';

class User{
    constructor( episode_id, title, opening_crawl){
        this.episode_id = episode_id;
        this.title = title;
        this.opening_crawl = opening_crawl;
        this.element = {
        wrapper: document.createElement("div"),
        }
    }
render() {
    const {wrapper} = this.element;
    wrapper.insertAdjacentHTML('afterbegin'),
    `
      <ul>
         <li>
             <h5>${this.name}</h5>
             <div>${this.episodeId}</div>
             <div>${this.openingCrawl}</div>
         </li>
      </ul>
    `
}

}

const data = () => fetch(urlApi)
  .then((response) => {
    return response.json();
  })
  .then(data => {
      data.forEach(film=>{
        const id = film.episodeId;
        renderMovie(film, id)
        getMovieCharacters(film.characters)
        .then(charactersList=>{
          console.log(charactersList);
            renderCharacters(charactersList, id)
        })
      })
    
  });

const ul = document.createElement('ul');

function renderMovie(film, id){
const li = document.createElement('li');
const div = document.createElement('div');
li.id = id;
li.innerHTML = `${film.name}`;
ul.append(li);
div.innerHTML = `${film.episodeId}<br>${film.openingCrawl}`;
li.append(div);

}
document.body.appendChild(ul);

  function getMovieCharacters(links){
    return new Promise((resolve, reject)=>{
        const characters = [];
        links.forEach(link=>{
            fetch(link)
            .then(response=>response.json())
            .then(character=>{
                characters.push(character.name);
                if(characters.length===links.length){
                    resolve(characters);
                }
            })
        })
    });

  }
  function renderCharacters(character, id){
    const episode = document.getElementById(id);
    const episodeInfo = episode.querySelector("div");
    const charactersList = document.createElement ("ul");
    episodeInfo.insertAdjacentElement('beforebegin', charactersList); 

    character.forEach(item=>{
      const characterItem = document.createElement("li");
      characterItem.innerHTML = item
      charactersList.appendChild(characterItem)
    })

  }


 data();




  

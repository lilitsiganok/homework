// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

// Эта констркуция используется тогда, нам надо будет использовать в коде полученную от пользователя информацию,
//  а тот в свою очередь пропустит или не заполнить какое-то поле, вот например как в случае с домешней работой.
//   Соответственно, нам надо будет отследить эту ошибку.
//  Или в случае если будет работать не корректно json! в этом случае код упадет, страница даже не сможет отобразится, поэтому тут нужен  
//   try {  
//   } catch (error) {  
//   }
// генерация собственных механических ошибок, когда случайно разработчик не добавил какое-то свойство (код отобразится но не корректро)


const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];



let booksItem = document.createElement("ul");
document.getElementById("root").append(booksItem);
console.log(root);

for (const item of books){
    try{
        
        const { author, name, price} = item;

if (!price) {
    throw new Error(`Price is not exist`);
}
if (!name) {
    throw new Error(`Name is not exist`);
}
if (!author) {
    throw new Error(`Author is not exist`);
}

        let booksEl = document.createElement("li");
        booksEl.innerText = `${author}: "${name}" ${price} uah`;
        booksItem.append(booksEl);
    }
    catch (error) {
console.error(error.message);
    }

};

// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript

// в процессе программирования очень часто надо использовать свойства уже имеющегося обьекта и немного его расширить.
// Чтобы нам не приходилось создавать каждый раз новый такой же обьект и прописывать каждое его свойство вручную, - и нужно прототипное наследование.
// Прототипное наследование - это свойство, которое использует свойства родительского елемента, не создавая его еще раз, а разширяя его новыми, нужными для дальнейшей работы свойствами.




class Employee {
    constructor (name,age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary
    }
    get name() {
       return this._name; 
       
    }
    set name(value){
        return this._name = value;
    }

    get age (){
        return this._age;
    }

    set age(value){
        return this._age = value;
    }

    get salary(){
        return this._salary;
    }

    set salary(value){
        return this._satary = value;
    }

 
}
const employee = new Employee ('Lili', 25, 25000)
console.log(employee);

class Programmer extends Employee{
constructor (name,age, salary, lang){
    super(name,age, salary);
    this._lang=lang
    
}

get salary(){
    return this._salary *3;
}
set salary(value){
    return this._salary = value;
}
}
const programmer = new Programmer ("Alex", 34, 22000, "js")
console.log(programmer); 
console.log(programmer.salary);

const programmer2 = new Programmer("Lola", 28, 30000, "java")
console.log(programmer2);
console.log(programmer2.salary);

const programmer3 = new Programmer("Nick", 31, 20000, "PHP")
console.log(programmer3);
console.log(programmer3.salary);

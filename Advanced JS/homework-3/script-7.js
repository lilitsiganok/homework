// Задание 7

// Дополните код так, чтоб он был рабочим

const array = ['value',  () => 'showValue'];
const value = array[0];
const showValue = array[1];

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'

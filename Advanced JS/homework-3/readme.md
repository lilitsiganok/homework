Обьясните своими словами, как вы понимаете, что такое деструктуризация и зачем она нужна?

Деструктуризация нужна для сокращения колличества кода. 
Тоисть это  назначение того же дествия кода, только более сокращенным вариантом при этом результат мы получим такой же.
Вот пример:

function calc (a,b) {
    return [a+b, a-b, a*b, a/b]
}

const result = calc(50, 10)
const sum = result[0]
const sub = result[1]
const multiple = result[2]
const division = result[3]

но вместо этого длинного кода мы можем написать ту же инструкцию, но короче:

const [sum, sub, multiple, division] = result

console.log(sum, sub, multiple, division)